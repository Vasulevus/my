<?php

if ( ! function_exists( 'vasulevus_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function vasulevus_setup() {

	/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );//

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'vasulevus_setup' );


function register_vasulevus_menus(){//Реєстрація меню
    register_nav_menus(array(
        'menu-header' => 'Primary',//основне меню
        'menu_social' => 'Social links'//соц мережі
    ));
};

add_action('after_setup_theme', 'register_vasulevus_menus');


function vasulevus_highbar_setup(){/*Сайдбар у самому верху - не схована лише кнопка*/
	$args = array(
		'name'=>'Highbar',
		'id'=>'highbar',
		'description'=>'contact form in top'
	);
	register_sidebar($args);
}

add_action('widgets_init','vasulevus_highbar_setup');


function vasulevus_enqueue_scripts(){//підключення стилів та скриптів
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_script('main-script', get_template_directory_uri() . '/js/script.js',array(),'1.0.0', true);
}

add_action('wp_enqueue_scripts', 'vasulevus_enqueue_scripts');//ініціалізація стилів та скриптів


add_action( 'after_setup_theme', 'vasulevus_image_sizes' );//Встановлення кастомних розмірів зображень
function vasulevus_image_sizes() {
	add_image_size( 'vs_gallery', 277, 277, true );
	add_image_size( 'vs_preview_thumbnail', 440, 270, true );

	add_image_size( 'vs_small_preview_thumbnail', 300, 200, true );

	add_image_size('vs-post-hero', 1600, 380, true);
}


?>