<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed|Roboto+Mono" rel="stylesheet">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class( ); ?>>


<div id="mySidenav" class="vs-sidenav vs-position-1">
  <a href="javascript:void(0)" class="closebtn" id="bug">&times;</a>
  <?php wp_nav_menu( array(
   'container' => false,
   'theme_location'  => 'menu_header',
   'menu_class'      => 'flexnav col-12 lg-screen',
   'items_wrap' =>'<ul data-breakpoint="991" id="%1$s" class="%2$s">%3$s</ul>'
)); ?>
</div>

<div id="mySidenav-2" class="vs-sidenav vs-position-2">
  <a href="javascript:void(0)" class="closebtn2" id="bug-2">&times;</a>

</div>

    <header class="vs-container vs-1600">
        <div class="vs-line vs-flex">
            <div class="vs-btn" id="but">меню</div>
          <div class="vs-btn" id="but-2">контакти</div>
        </div>
        <div class="vs-separator"></div>
        <div class="vs-name vs-rotate">
                <div class="card">
                  <div class="back">VASULEVUS</div>
                </div>
            </div>
            <div class="vs-separator"></div>

    </header>