<?php get_header(); ?>

                            <?php if(have_posts()):
                            while(have_posts()): the_post();
                            ?>
                            <div class="vs-container vs-flex-column vs-1600">
                            <div class="vs-post-hero"><?php the_post_thumbnail('vs-post-hero'); ?></div>
                       <div class="vs-post-title"><?php the_title();?></div>

                        <?php the_content();?>

                        </div>
                        <?php endwhile; endif; ?>
   
<?php get_footer(); ?>