<?php get_header(); ?>
<main>
    <div class="vs-container vs-1600">
    <h1>Статті</h1>
    <div class="vs-main-grid">

        <?php
        $cl=0;
        if(have_posts()):
            while(have_posts()): the_post(); $cl++?>
            <div class="vs-card vs-card-<?php echo $cl; ?>">
                <div class="vs-thumb-image vs-thumb-image-<?php echo $cl; ?>">


  
		<a href="<?php the_permalink();?>">
			<?php the_post_thumbnail('vs_preview_thumbnail'); ?>
		</a>
        </div><!--vs-thumb-image vs-thumb-image-$-->
        <div class="vs-post-title vs-post-title-<?php echo $cl; ?>">
			<a href="<?php the_permalink();?>"><?php the_title();?></a>
            </div>
            <div class="vs-excerpt vs-excerpt-<?php echo $cl; ?>">
            <?php the_excerpt(10); ?>
            </div>
<div class="vs-date vs-date-<?php echo $cl; ?>"><?php the_time('j F Y');?></div>
</div><!--vs-card vs-card-$-->
<?php    endwhile;?>
</div><!--vs-main-grid-->
<div class="pag"><?php the_posts_pagination( array(
	             'mid_size' => 1,
	             'prev_text' => __( 'Prev', 'textdomain' ),
	             'next_text' => __( 'Next', 'textdomain' ),
             ) );?>
      <?php endif; ?>
      </div>
      </div><!--vs-container vs-1600-->
      </main>


<?php get_footer(); ?>